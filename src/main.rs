use std::process;

use arg_dot_h::*;

fn help(argv0: &str) {
    eprintln!("{} [file ...]", argv0);
    process::exit(1);
}

fn main() {
    let mut argv0 = String::new();
    let filenames = argbegin! {
        &mut argv0,
        _ => help(&argv0)
    };

    editor::run(filenames);
}
