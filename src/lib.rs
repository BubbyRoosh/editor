use std::error::Error;
use std::fs;

use ncurses::*;

pub mod binds;

static BARHEIGHT: i32 = 2;

pub enum Mode {
    Normal,
    Insert,
    Input,
}

pub struct Buffer {
    pub filename: String,
    pub mode: Mode,
    pub lines: Vec<String>,
    pub cx: usize,
    pub cy: usize,
    pub sy: usize,
    pub sel_begin: usize,
    pub sel_end: usize,
    pub should_clear: bool,
    pub cmd: String,
    pub clipboard: Vec<String>,
    pub modified: bool,
}

impl Buffer {
    fn new() -> Self {
        Self {
            filename: String::new(),
            mode: Mode::Normal,
            lines: vec!(String::new()),
            cx: 0,
            cy: 0,
            sy: 0,
            sel_begin: 0,
            sel_end: 0,
            should_clear: true,
            cmd: String::new(),
            clipboard: Vec::new(),
            modified: false,
        }
    }

    fn from(filename: &str) -> Result<Self, Box<dyn Error>> {
        Ok(Self {
            filename: String::from(filename),
            mode: Mode::Normal,
            lines: fs::read_to_string(filename)?.lines().map(|l| l.to_string()).collect(),
            cx: 0,
            cy: 0,
            sy: 0,
            sel_begin: 0,
            sel_end: 0,
            should_clear: true,
            cmd: String::new(),
            clipboard: Vec::new(),
            modified: false,
        })
    }

    pub fn get_line(&mut self, idx: usize) -> &mut String {
        &mut self.lines[idx]
    }

    pub fn cur_line(&mut self) -> &mut String {
        &mut self.lines[self.sy + self.cy as usize]
    }

    pub fn remove_line(&mut self, idx: usize) -> String {
        if idx > self.lines.len() - 1 {
            String::new()
        } else {
            self.modified = true;
            if self.lines.len() == 1 {self.lines.push(String::new())}
            self.lines.remove(idx)
        }
    }

    pub fn add_line(&mut self, idx: usize, line: String) {
        if idx > self.lines.len() {
            self.lines.push(line);
        } else {
            self.lines.insert(idx, line);
        }
    }

    pub fn insert(&mut self, ch: char) {
        if self.cy > self.lines.len() {self.cy = self.lines.len() - 1; self.cx = self.get_line(self.lines.len() - 1).len()}

        let mut cx = self.cx;
        let cur_line = self.cur_line();
        if cx > cur_line.len() {cx = cur_line.len()}
        cur_line.insert(cx as usize, ch);
        cx+=1;
        self.cx = cx;
        self.modified = true;
    }

    pub fn back(&mut self) {
        let cx = self.cx;
        let cy = self.cy+self.sy;
        let cur_line = self.cur_line();
        if cx > cur_line.len() {
            cur_line.pop();
            self.cx = cur_line.len();
        } else if cx > 0 {
            cur_line.remove(cx as usize - 1);
            self.cx-=1;
        } else if cy > 0 {
            let cl = cur_line.clone();
            self.cx = self.get_line(cy-1).len();
            self.get_line(cy-1).push_str(&cl);
            self.remove_line(cy);
            self.cy-=1;
        }
        self.modified = true;
    }

    pub fn move_cursor(&mut self, y: i32, x: i32) {
        let mut cy = self.cy as i32;
        let mut cx = self.cx as i32;
        cy += y;
        cx += x;

        /*
         * Btw if you're reading this and wondering why I do "> ....len() - 1" instead of ">=
         * ....len()" it's cause it makes more sense. len() returns the number total *starting* at
         * 1 (not 0)
         */
        if cy + self.sy as i32 > self.lines.len() as i32 - 1 && self.lines.len() > 1 {
            self.cy-=1;
        }
        if cy > LINES() - BARHEIGHT - 1 {
            self.sy+=1;
            cy = LINES() - BARHEIGHT - 1;
        } else if cy < 0 && self.sy > 0 {
            self.sy-=1;
        }
        if cy < 0 {cy = 0}
        if cx < 0 {cx = 0}
        self.cy = cy as usize;
        self.cx = cx as usize;
    }

    pub fn save(&mut self) {
        if self.filename.is_empty() {
            self.filename = self.prompt("Name: ");
        }
        match fs::write(&self.filename, self.lines.join("\n")) {
            Ok(()) => self.modified = false,
            Err(e) => {self.prompt(&format!("Error trying to save: {}", e));},
        }
    }

    fn draw_bar(&self, idx: usize) {
        for i in 0..BARHEIGHT {
            mv(LINES() - BARHEIGHT - i + 1, 0);
            clrtoeol();
        }

        mvprintw(LINES() - BARHEIGHT, 0, &format!("File: {}; Line: {}, Col: {}; Buffer: {}",
                if self.modified {format!("{}{}", &self.filename, '*')} else {self.filename.clone()},
                self.cy,
                self.cx,
                idx
        ));
    }

    fn draw(&self, idx: usize) {
        if self.should_clear {
            clear();
        }
        match self.mode {
            Mode::Input => {},
            _ => {
                let sy = self.sy;
                for (idx, line) in self.lines.iter().enumerate() {
                    if idx + 1 > sy && idx < sy + (LINES() - 2) as usize {
                        mvprintw((idx - sy) as i32, 0, line);
                    }
                }
            },
        }
        self.draw_bar(idx);
        mv(self.cy as i32, self.cx as i32);
    }

    fn prompt(&self, prompt: &str) -> String {
        mvprintw(LINES() - 1, 0, prompt);
        let mut input = String::new();
        echo();
        let mut key = getch();
        while key != 10 {
            if key == 127 {
                input.pop();
            } else {
                input.push(key as u8 as char);
            }
            key = getch();
        }
        noecho();
        input
    }
}

pub struct Editor {
    pub buffers: Vec<Buffer>,
    pub cur_buffer: usize,
}

impl Editor {
    fn new() -> Self {
        Self {
            buffers: vec!(Buffer::new()),
            cur_buffer: 0,
        }
    }

    fn from(filenames: Vec<String>) -> Self {
        let mut buffers = Vec::new();
        for name in filenames {
            match Buffer::from(&name) {
                Ok(b) => buffers.push(b),
                Err(e) => eprintln!("Error opening {}: {}", name, e),
            }
        }

        Self {
            buffers,
            cur_buffer: 0,
        }
    }

    fn handle_keypress(&mut self, key: char) {
        binds::handle_keypress(self, key);
    }

    fn run(&mut self) {
        let mut key;
        loop {
            self.buffers[self.cur_buffer].draw(self.cur_buffer);
            key = getch() as u8 as char;
            self.handle_keypress(key);
        }
    }
}

// TODO: Config through ketos
pub fn run(filenames: Vec<String>) {
    let mut editor = if filenames.is_empty() {
        Editor::new()
    } else {
        Editor::from(filenames)
    };

    initscr();
    noecho();
    cbreak();

    editor.run();

    echo();
    nocbreak();
    endwin();
}