use super::*;

fn new_line(buf: &mut Buffer, new_string: Option<String>) {
    let mut cx = buf.cx;
    let mut cy = buf.cy + buf.sy;
    let cur_line = buf.cur_line();
    if cx > cur_line.len() {cx = cur_line.len();}
    let past_cursor = if let Some(ns) = new_string {ns} else {cur_line.split_off(cx as usize)};
        
    cy += 1;
    let mut cloned = cur_line.clone().chars().rev().collect::<String>();
    buf.add_line(cy as usize, past_cursor);

    buf.cy=cy - buf.sy;
    cx = 0;

    let cur_line = buf.cur_line();
    while cloned.ends_with(' ') {
        *cur_line = format!(" {}", cur_line);
        cx+=1;
        cloned.pop();
    }

    buf.modified = true;
    buf.cx = cx;
}

pub fn handle_keypress(editor: &mut Editor, key: char) {
    let mut buf = &mut editor.buffers[editor.cur_buffer];
    let mut bact = 0;
    match buf.mode {
        Mode::Normal => {
            buf.cmd.push(key);
            let mut matched = true;
            match buf.cmd.as_str() {
                // Dec buffer.
                "-" => bact-=1,
                // Inc buffer.
                "+" => bact+=1,
                // Enter insert mode.
                "i" => buf.mode = Mode::Insert,
                // Enter insert mode 1 char in front of cursor.
                "a" => {buf.mode = Mode::Insert; buf.cx+=1},
                // Enter insert mode at end of line.
                "A" => {buf.mode = Mode::Insert; buf.cx = buf.cur_line().len();},
                // Enter insert mode at first non-space char of line.
                "I" => {
                    buf.mode = Mode::Insert;
                    match buf.cur_line().chars().position(|ch| ch != ' ') {
                        Some(idx) => buf.cx = idx,
                        None => buf.cx = 0
                    }
                },
                // Make a new line and enter insert mode on it.
                "o" => {buf.mode = Mode::Insert; new_line(&mut buf, Some(String::new()))},
                // Enter Input mode (for testing key input)
                "|" => {buf.mode = Mode::Input; buf.should_clear = false;},
                // Move cursor left 1.
                "h" => buf.move_cursor( 0,  -1),
                // Move cursor down 1.
                "j" => buf.move_cursor( 1,  0),
                // Move cursor up 1.
                "k" => buf.move_cursor(-1,  0),
                // Move cursor right 1.
                "l" => buf.move_cursor( 0,  1),
                // Go to beginning of line.
                "0" => buf.cx = 0,
                // Go to end of line.
                "$" => buf.cx = buf.cur_line().len(),
                // Scroll down 1.
                "J" => buf.sy+=1,
                // Scroll up 1.
                "K" => if buf.sy > 0 {buf.sy-=1},
                // Save the file.
                "W" => buf.save(),
                // Rename the file.
                "R" => buf.filename = buf.prompt("Name: "),
                // Begin the selection (full line)
                "sb" => buf.sel_begin = buf.cy+buf.sy,
                // End the selection (full line)
                "se" => buf.sel_end = buf.cy+buf.sy+1,
                // Copy the selection.
                "sy" => {
                    if buf.sel_begin > buf.sel_end {
                        let tmp = buf.sel_begin;
                        buf.sel_begin = buf.sel_end;
                        buf.sel_end = tmp;
                    }
                    buf.clipboard = buf.lines[buf.sel_begin..buf.sel_end].iter().map(|l| l.clone()).collect::<Vec<_>>();
                },
                // Cut the selection
                "sd" => {
                    if buf.sel_begin > buf.sel_end {
                        let tmp = buf.sel_begin;
                        buf.sel_begin = buf.sel_end;
                        buf.sel_end = tmp;
                    }
                    let mut tmp = Vec::new();
                    for _ in buf.sel_begin..buf.sel_end {
                        tmp.push(buf.remove_line(buf.sel_begin));
                    }
                    buf.clipboard = tmp;
                },
                // Remove the current char the cursor is on.
                "x" => {
                    buf.cx += 1;
                    buf.back();
                    if buf.cx > buf.cur_line().len() {buf.cx = buf.cur_line().len();}
                },
                // Copy the current line to the clipboard.
                "yy" => buf.clipboard = vec!(buf.cur_line().to_string()),
                // Cut the current line.
                "dd" => {
                    let mut cy = buf.cy;
                    if cy == buf.lines.len() {
                        cy-=1;
                    }
                    buf.clipboard = vec!(buf.remove_line(cy + buf.sy));
                },
                // Cut to the end of the line starting at the cursor's position.
                "D" => {
                    let cx = buf.cx;
                    buf.clipboard = vec!(buf.cur_line().split_off(cx));
                },
                // Paste the contents of the clipboard.
                "p" => {
                    let cb = buf.clipboard.clone();
                    for line in cb.iter().rev().map(|l| l.to_string()) {
                        buf.add_line(buf.cy+buf.sy, line);
                    }
                },
                _ => matched = false,
            }
            // TODO: Less crude
            if buf.cmd.len() > 3 || matched {buf.cmd.clear();}
        },
        Mode::Insert => {
            let k = key as u8;
            match k {
                /* Tab */
                9 => {
                    let mut cx = buf.cx;
                    let cur_line = buf.cur_line();
                    if cx > cur_line.len() {cx = cur_line.len();}
                    buf.cur_line().insert_str(cx, "    ");
                    buf.cx += 4;
                },
                /* Enter */
                10 => {
                    new_line(&mut buf, None);
                },
                /* Escape */
                27 => buf.mode = Mode::Normal,
                /* Backspace */
                127 => buf.back(),
                _ => buf.insert(key),
            }
        },
        Mode::Input => {
            match key {
                '|' => {buf.mode = Mode::Normal; buf.should_clear = true;},
                _ => {clear(); mvprintw(0, 0, &format!("{} {}", key.to_string(), (key as u8).to_string()));},
            }
        }
    }

    // Can't be in the actual stuff cause mutable borrows and stuff /shrug
    if bact == 1 {
        if editor.cur_buffer == editor.buffers.len() - 1 {
            editor.buffers.push(Buffer::new());
        }
        editor.cur_buffer+=1;
    } else if bact == -1 && editor.cur_buffer > 0 {
        editor.cur_buffer-=1;
    }
}